<?php
include("connection.php");

	$sql = "SELECT * FROM film";
	$result = $mysqli->query($sql);
	$result_array = array();

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			array_push($result_array, $row);
		}
	}
	echo json_encode($result_array);
	$mysqli->close();
?>