/**
 * File : reco.js
 * @file This file contain all method for update recommendation for the current film
 * @copyright Theo ZERIBI 2021
 */

/**
 * The userId for saving recommendation in DB.
 * User is randomly generate between 1 and 6 at each page refresh.
 * @type {number}
 * */
let userId = Math.floor(Math.random() * 5) + 1;

/**
 * Request for update specific recommendation category
 * 
 * @param {string} column - The recommendation category
 * @param {number} note - The note for recommendation category
 * @throws Print error in console
 */
function updateReco(column, note) {
	$.ajax({
			method: "GET",
			data: {
				user_id: userId,
				column: column,
				film_id: cFilm['id'],
				note: note
			},
			url: "./ressources/php/update_reco.php",
			success: () => getRecoById(cFilm['id']).then ((response) => showReco(cFilm, response)),
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(`ErrorStatus : ${textStatus}`);
				console.log(`Error : ${errorThrown}`);
				console.log(XMLHttpRequest);
			},
		});
}

/**
 * Method for initialize event trigger for all recommendation star
 * If event is trig, the trigger will get the note and which recommendation category is it
 * The name of the recommendation category will be correctly translated to match the names of the columns in the DB
 */
function initRecoTrig() {
	$(".film-star").click(function(){
		let note = $(this).attr('id');
		let parent = $(this).parent();
		let reco = parent.attr('id');
		let column;

		switch (reco) {
			case "origin":
				column = "originality";
				break;
			case "scenar":
				column = "scenario";
				break;
			case "actor":
				column = "actor"
				break;
			case "scene":
				column = "staging";
				break;
			case "sound":
				column = "soundtrack";
				break;
		}

		if (column == undefined) return;

		updateReco(column, parseInt(note) + 1);
 });
}
