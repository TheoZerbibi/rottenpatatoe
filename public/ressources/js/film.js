/**
 * File : film.js
 * @file This file contain all method for get a film or its recommendation
 * @copyright Theo ZERIBI 2021
 */

/**
 * Using for contain all recommendation for current Film
 * @type {Array}
 * */
let filmReco = {};

/**
 * Contain current Film
 * @type {Array}
 * */
let cFilm;

/**
 * Request for getting specific film by is ID
 * 
 * @param {number} id - The film id
 * @return {Promise} - Promise contain the result in array of the SQL Query
 */
function getFilmById(film_id) {
		return Promise.resolve($.ajax({
			method: "GET",
			data: {
				id : film_id
			},
			url: "./ressources/php/film.php",
		}));
	}

/**
 * Request for getting recommendation for current Film by his film id
 * 
 * @param {number} id - The film id
 * @return {Promise} - Promise contain the result in array of the SQL Query
 */
function getRecoById(film_id) {
	return Promise.resolve($.ajax({
		method: "GET",
		data: {
			id : film_id
		},
		url: "./ressources/php/reco.php",
	}));
}

/**
 * Method for calculate and print star for all recommendation category
 * 
 * @param {string} div - The div for recommendation category
 * @param {number} nbr - Number of star for this recommendation category
 */
function printStar(div, nbr)
{
	if (nbr == NaN)
		nbr = 0;
	if (nbr == -1)
		nbr = Math.floor(Math.random() * 6);
	if (nbr > 5) nbr = 5;
	for (let i = 0; i < nbr; i++)
		$(`${div}`).append(`<img class="film-star" id="${i}" src="ressources/img/film/star.png"/>`);
	if (nbr == 5) return;
	for (let i = nbr; i < 5; i++)
		$(`${div}`).append(`<img class="film-star" id="${i}" src="ressources/img/film/unstar.png"/>`);
}

/**
 * Method for calculate recommendation note for current film
 * 
 * @param {Array} recoArr - The Array which contains all recommendation for current film
 * @return {Enum} - The Enum which contains all calculate recommendation notes
 */
function caclReco(recoArr) {
	let globalNote = originNote = scenarNote = actorNote = sceneNote = soundNote = 0;
	let origin = [], scenar = [], actor = [], scene = [], sound = [];
	if (recoArr.length > 0) {
		for (reco of recoArr) {
			if (parseInt(reco['originality']) > 0)
				origin.push(parseInt(reco['originality']));
			if (parseInt(reco['scenario']) > 0)
				scenar.push(parseInt(reco['scenario']));
			if (parseInt(reco['actor']) > 0)
				actor.push(parseInt(reco['actor']));
			if (parseInt(reco['staging']) > 0)
				scene.push(parseInt(reco['staging']));
			if (parseInt(reco['soundtrack']) > 0)
				sound.push(parseInt(reco['soundtrack']));
		}
		if (origin.length > 0)
			originNote = Math.floor(origin.reduce((a, b) => a + b, 0) / origin.length);
		if (scenar.length > 0)
			scenarNote = Math.floor(scenar.reduce((a, b) => a + b, 0) / scenar.length);
		if (actor.length > 0)
			actorNote = Math.floor(actor.reduce((a, b) => a + b, 0) / actor.length);
		if (scene.length > 0)
			sceneNote = Math.floor(scene.reduce((a, b) => a + b, 0) / scene.length);
		if (sound.length > 0)
			soundNote = Math.floor(sound.reduce((a, b) => a + b, 0) / sound.length);
	}
	globalNote = Math.floor((originNote + scenarNote + actorNote + sceneNote + soundNote) / 5);
	return {GLOBAL: globalNote, ORIGIN: originNote, SCENAR: scenarNote, ACTOR: actorNote, SCENE: sceneNote, SOUND: soundNote};
}

/**
 * Method print all recommendation note
 * 
 * @param {Array} film - Array contains the current Film
 * @param {Array} reco - Array contains recommendation note for the current Film
 */
function showReco(film, reco) {
	const Reco = caclReco(reco);

	$('#side').empty();
	$('#side').append(`<img class="film-title" src="ressources/img/film/title/${film['image']}-title.png" />`);
	$('#side').append('<div class="reco" id="global">');
	$('#global').append('<p>Note global &nbsp;&nbsp;:</p>');
	printStar("#global", Reco.GLOBAL);
	$('#global').append('</div>');

	$('#side').append('<div class="reco" id="origin">');
	$('#origin').append('<p>Originalité &nbsp;&nbsp;&nbsp; :</p>');
	printStar("#origin", Reco.ORIGIN);
	$('#origin').append('</div>');

	$('#side').append('<div class="reco" id="scenar">');
	$('#scenar').append('<p>Scénario &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;:</p>');
	printStar("#scenar", Reco.SCENAR);
	$('#scenar').append('</div>');

	$('#side').append('<div class="reco" id="actor">');
	$('#actor').append('<p>Jeu d\'acteurs &nbsp;&nbsp;:</p>');
	printStar("#actor", Reco.ACTOR);
	$('#actor').append('</div>');

	$('#side').append('<div class="reco" id="scene">');
	$('#scene').append('<p>Mise en scène :</p>');
	printStar("#scene", Reco.SCENE);
	$('#scene').append('</div>');

	$('#side').append('<div class="reco" id="sound">');
	$('#sound').append('<p>Bande son &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>');
	printStar("#sound", Reco.SOUND);
	$('#sound').append('</div>');

	if (Reco.ACTOR + Reco.ORIGIN + Reco.SCENAR + Reco.SCENE + Reco.SOUND <= 0)
		$('#side').append(`<br /><h1>Il n\'y a pas encore de vote pour ${film['name']} !<br/>Votre avis nous intéresse, soyez le premier a voter !</h1>`);
	if (Reco.GLOBAL == 5)
		$('#side').append(`<br /><h2>Ce film a 5 étoiles !</h2>`);
	if (film['id'] == 3)
		$('#side').append(`<br /><h3>Regardez ce film, il est vraiment incroyable.</h3>`);

	$('#side').append('</div>');
	$('#film').append(`</div>`);

	initRecoTrig();
}

/**
 * Method for show all value for current film (Poster Film, Title, recommendation)
 * 
 * @param {Array} film - Array contains the current Film
 */
function showFilm(film) {
	cFilm = film;
	$('#film').empty();
	$('#film').addClass('film');
	$('#film').prepend(`<img class="film-img" src="ressources/img/film/poster/${film['image']}.jpg" />`);
	$('#film').append(`<iframe class="film-video" src="https://www.youtube.com/embed/${film['link']}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`);
	$('#film').append('<div class="side" id="side">');

	getRecoById(film['id']).then ((response) => showReco(film, response));
}

