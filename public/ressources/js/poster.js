
/**
 * File : poster.js
 * @file This file contain all method for get all poster Film and display them
 * @copyright Theo ZERIBI 2021
 */
$(document).ready(() => {

	/**
	* Method for initialize event trigger for all film poster
	* If event is trig, the trigger will get the film id and call getFilmById and showFilm method
	*/
	function initPosterTrig() {
		$(".poster-img").click(function(){
			let id = $(this).attr('id');
			getFilmById(id).then((response) => showFilm(response[0]));
		});
	}

	/**
	* Request for get all film poster
	* 
	* @return {Promise} - Promise contain the result in array of the SQL Query
	*/
	function getPoster() {
		return Promise.resolve($.ajax({
			method: "GET",
			url: "./ressources/php/poster.php",
		}));
	}

	/**
	* Method for display all film poster
	* 
	* @param {Array} id - Arrays which contains all poster
	*/
	async function showPoster(posterArr) {
		let pathx = "ressources/img/film/poster/";
		$('#poster').empty();
		for (poster of posterArr) {
			$('#poster').append(`<img class="poster-img" src="${pathx}${poster['image']}.jpg" id="${poster['id']}" />`);
		}
		initPosterTrig();
	}


	getPoster().then (response => showPoster(response));
});