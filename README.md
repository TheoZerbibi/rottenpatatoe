### Rotten_Patatoe

# Let's get started!

**On your Local Computer**: in your terminal, connect to your mysql service and lauch the init_project.sql script

After that, go in public/ folder and type :

```
$ php -S 127.0.0.1:8000
```

Then you can go to [http://localhost:8000](http://localhost:8000) to see your new website in action!

## File Structure

```
public/                         Main Folder
----
----index.html                  Main HTML Page of entire website
----
----ressources/                 Folder contains all ressources files
---------------js/              Folder of .js javascript files
---------------css/             Folder of .css stylesheet files
---------------php/             Folder of .php file
---------------                 PHP files are mainly used as script used by Ajax Jquery requests
---------------img/             Contains all image used for this site
------------------film/         Contains all film image
----------------------poster/   Contains all image poster for each films
----------------------title/    Contains all title image for each films
------------------
------------------logo/         Contains favicon + logo site
------------------user/         Contains user profile picture
```
