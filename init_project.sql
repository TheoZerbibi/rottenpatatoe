-- Create DataBase and select it
CREATE DATABASE Rotten_Patatoe;
USE Rotten_Patatoe;

-- Create film table and fill it
create table film
(
    id    int auto_increment
        primary key,
    name  varchar(255) not null,
    image varchar(255) not null,
    link  text         not null
);

INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (1, 'Interstellar', 'interstellar', 'VaOijhK3CRU');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (2, 'Eva', 'eva', 'GiCQeHf3U0M');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (3, 'Morse', 'morse', 'ICp4g9p_rgo');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (4, 'Harry Potter
et le Prince de Sang-Mêlé', 'hp-6', 'yb-VYG3x5E8');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (5, 'Arcane', 'arcane', 'v-dgpzE7txk');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (6, 'HANNA', 'hanna', 'TWnmI3lGGhs');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (7, 'Magi:
The Labyrinth of Magic', 'magi', '2E7o26G1T0c');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (8, 'Bleach', 'bleach', '_ty-Nqm4Pdc');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (9, 'Spider-Man : No Way Home', 'spiderman', '7w_w10HVa54');
INSERT INTO Rotten_Patatoe.film (id, name, image, link) VALUES (10, 'Moi, quand je me réincarne en Slime', 'slime', '8dDSI60CpVo');

-- Create user table and fill it
create table user
(
    id       int auto_increment
        primary key,
    password text         not null,
    login    varchar(255) not null
);

INSERT INTO Rotten_Patatoe.user (id, password, login) VALUES (1, '81dc9bdb52d04dc20036dbd8313ed055', 'admin');
INSERT INTO Rotten_Patatoe.user (id, password, login) VALUES (2, '81dc9bdb52d04dc20036dbd8313ed055', 'patatoe');
INSERT INTO Rotten_Patatoe.user (id, password, login) VALUES (3, '81dc9bdb52d04dc20036dbd8313ed055', 'tomato');
INSERT INTO Rotten_Patatoe.user (id, password, login) VALUES (4, '81dc9bdb52d04dc20036dbd8313ed055', 'apple');
INSERT INTO Rotten_Patatoe.user (id, password, login) VALUES (5, '81dc9bdb52d04dc20036dbd8313ed055', 'carrot');

-- Create recommendation table
create table recommendation
(
    id          int auto_increment,
    user_id     int           null,
    film_id     int           null,
    originality int default 0 null,
    scenario    int default 0 null,
    actor       int default 0 null,
    staging     int default 0 null,
    soundtrack  int default 0 null,
    constraint recommendation_id_uindex
        unique (id),
    constraint recommendation_film_id_fk
        foreign key (film_id) references film (id),
    constraint recommendation_user_id_fk
        foreign key (user_id) references user (id)
);