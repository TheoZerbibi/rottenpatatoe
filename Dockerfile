FROM php:8-fpm

RUN docker-php-ext-install mysqli

WORKDIR /app/public

ENTRYPOINT [ "php", "-S", "0.0.0.0:8000" ]